##############################################################################
# Project: CHI - Joint Replacement - Tacoma/Chatt/Lincoln/Englewood
# Date: 9/22/14
# Author: Sam Bussmann
# Description: Bin National Market Data from saved formats
# Notes: Used this 
##############################################################################

### Handle missings in Ntl Mkt Data, also change Y/N to 1/0

load("natlmkt.RData")

### Change some names to match IRM data
dimnames(addr2)[[2]][which(names(addr2)=="presenceofchildren")]<-"preschild_hosp"
dimnames(addr2)[[2]][which(names(addr2)=="nummberofchildren")]<-"NumberOfChildren"
dimnames(addr2)[[2]][which(names(addr2)=="ownrent")]<-"OwnRent"
dimnames(addr2)[[2]][which(names(addr2)=="householdmembercount")]<-"HouseholdMemberCount"

#addr2<-addr2[1:10000,]
system.time(
for (i in 1:dim(addr2)[2]){
  temp<-unique(addr2[,i]) 
  if (sum(temp %in% c("Y","N"))==length(temp)){
    addr2[,i]<-ifelse(addr2[,i]=="Y","1","0")
    addr2[,i]<-as.integer(addr2[,i])
  }
  if (is.character(addr2[,i])) addr2[[i]][addr2[[i]] %in% c(" ","")]<-" BLANK"
}
)
### Pull the filter formats and check them

filternm<-list.files(file.path("F:/CHS/CHS.Joint.Replacement.7.30.2014", "filters"))

for (i in 1:length(filternm)){
  ## Check name exists in national market
  if (!(filternm[i] %in% names(addr2))) {print(filternm[i]); break}
  ## Get list of unique values in national market var
  temp2<-unique(addr2[,filternm[i]]) 
  ## Check if all unique values in national market var are also in original filter
  if (!(sum(temp2 %in% read.table(file.path("F:/CHS/CHS.Joint.Replacement.7.30.2014",
                                            "filters",filternm[i]),header=T)$values)==length(temp2))) {
    print(filternm[i])
  }
}

### Pull the standardized vars. Check existing and apply.


stdzd1<-list.files(file.path("F:/CHS/CHS.Joint.Replacement.7.30.2014", "standardized"))
stdzd<-stdzd1[-which(stdzd1=="MinDist")]

#check for NA's 
for (i in 1:length(stdzd)) print(summary(addr2[,stdzd[i]]))
sum(is.na(addr2[,stdzd[16]]))
addr2[is.na(addr2[,stdzd[16]]),stdzd[16]]<-mean(addr2[!is.na(addr2[,stdzd[16]]),stdzd[16]])

for (i in 1:length(stdzd)){
## Read mean and std
temp3<-read.table(file.path("F:/CHS/CHS.Joint.Replacement.7.30.2014","standardized",stdzd[i]),header=T)
## Check var exists in natl mkt
if (!(stdzd[i] %in% names(addr2))) {print(stdzd[i]); break}
## Check var has reasonable values
if (sum(is.na(addr2[,stdzd[i]]))>0) {addr2[,stdzd[i]][is.na(addr2[,stdzd[i]])]<-
                                       mean(addr2[,stdzd[i]],na.rm=T)}
## Apply standardization
addr2[,stdzd[i]]<-(addr2[,stdzd[i]]-temp3$mean)/temp3$stdev
## Change name to reflect standardization
dimnames(addr2)[[2]][which(names(addr2)==stdzd[i])]<-paste0(stdzd[i],".std")
}

### Find MinDist for these records and apply

hosploc_chs<-read.csv("hosp_address.csv")
hosploc_chs1<-data.frame(hosploc_chs$Facility,gGeoCode(as.character(hosploc_chs$Address)))

library(fields)
dist1<-rdist.earth(addr2[,c("longitude","latitude")],hosploc_chs1[,c("lng","lat")])
mindist<-do.call(pmin,data.frame(dist1))
# min_index<-adply(t(dist1),2,which.min)$V1  ### try sapply?
# mindist<-dist1[cbind(1:dim(addr2)[1],min_index)]
hist(mindist)
# by(mindist,min_index,summary)

## Check missing distances
sum(is.na(mindist))

## Now standardize MinDist
temp4<-read.table(file.path("F:/CHS/CHS.Joint.Replacement.7.30.2014","standardized","MinDist"),header=T)
addr2[,"MinDist.std"]<-(mindist-temp4$mean)/temp4$stdev

#plot(addr2$MinDist.std,tog$score,pch=".")

### Now Read formats, check levels, apply binning

fmts<-list.files(file.path("F:/CHS/CHS.Joint.Replacement.7.30.2014", "formats"))

## Check if there are any missings
for (i in 1:length(fmts)) if (sum(is.na(addr2[,fmts[i]]))>0) 
  print(paste("var=",fmts[i],", missings=",sum(is.na(addr2[,fmts[i]]))))

for (i in 1:length(fmts)){
  ## Read the format
  temp5<-read.table(file.path("F:/CHS/CHS.Joint.Replacement.7.30.2014",
                              "formats",fmts[i]),header=T,colClasses="character")
  ## Check name exists in national market
  if (!(fmts[i] %in% names(addr2))) {print(fmts[i]); break}
  ## Get list of unique values in national market var
  temp6<-as.character(unique(addr2[,fmts[i]])) 
  ## Check if all unique values in national market var are also in original filter
  if (!(sum(temp6 %in% temp5$start)==length(temp6))) {print(fmts[i]); break}
  ## Apply levels
  id<-match(as.character(addr2[,fmts[i]]),temp5$start)
  addr2[,paste0(fmts[i],".bin")]<-temp5$finish[id]
}
table(addr2[,fmts[i]],useNA="ifany")

### Now handle the conditionals
### Edited add function to make this easier in the future
### maybe could add all integers less than max of range and use lookup table as directly above.

# cdls<-list.files(file.path(getwd(), "conditionals"))
# for (i in 1:length(cdls)) print(paste("var=",i,"missings=",sum(is.na(addr2[,cdls[i]]))))
# 
# temp7<-read.table(file.path(getwd(),"conditionals",cdls[i]),header=T,colClasses="character")

## Homeage
addr2$homeage.bin<-ifelse(addr2[,"homeage"]>55,"55+",
           ifelse(addr2[,"homeage"]>40,"41-55",
                  ifelse(addr2[,"homeage"]>25,"26-40",
                         ifelse(addr2[,"homeage"]>0,"1-25","0"))))

## lotsize

addr2$lotsize.bin<-ifelse(addr2[,"lotsize"]>300,"300+",
            ifelse(addr2[,"lotsize"]>0,"1-300","Unknown"))

## Mortgageinterestrate

addr2$mortgageintrestrate.bin<-ifelse(is.na(addr2[,"mortgageintrestrate"])," Missing",
                         ifelse(addr2[,"mortgageintrestrate"]<430,"<4.3",
                                ifelse(addr2[,"mortgageintrestrate"]<500,"[4.3-5)",   
                                       ifelse(addr2[,"mortgageintrestrate"]<600,"[5-6)",
                                              ifelse(addr2[,"mortgageintrestrate"]<700,"[6-7)","[7+)")))))

##sesi

addr2$sesi.bin<-ifelse(addr2[,"sesi"]>55,"56 - 91",
                          ifelse(addr2[,"sesi"]>39,"40 - 55","16 - 39"))

## buyer behavior cluster

addr2$buyer_behavior_cluster_2004.bin<-ifelse(addr2[,"buyer_behavior_cluster_2004"]>33,"34 - 51",
                       ifelse(addr2[,"buyer_behavior_cluster_2004"]>18,"19 - 33","1 - 18"))

## csi_raw_score


addr2$csi_raw_score.bin<-ifelse(addr2[,"csi_raw_score"]>74,"75 - 00",
                                              ifelse(addr2[,"csi_raw_score"]>62,"63 - 74","0 - 62"))